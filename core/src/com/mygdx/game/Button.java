package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Button {

    float x;
    float y;
    float w;
    float h;
    boolean isDown = false;
    boolean isDownPrev = false;

    Texture textureUp;
    Texture textureDown;

    /**
     * Button constructor
     * @param x
     * @param y
     * @param w
     * @param h
     * @param textureUp
     * @param textureDown
     */
    public Button(float x, float y, float w, float h, Texture textureUp, Texture textureDown) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;

        this.textureUp = textureUp;
        this.textureDown = textureDown;
    }

    /**
     * Updates the button to check if it has been pressed
     * @param checkTouch
     * @param touchX
     * @param touchY
     */
    public void update(boolean checkTouch, int touchX, int touchY) {
        isDown = false;

        if (checkTouch) {
            int h2 = Gdx.graphics.getHeight();
            //Touch coordinates have origin in top-left instead of bottom left
            isDownPrev = isDown;
            if (touchX >= x && touchX <= x + w && h2 - touchY >= y && h2 - touchY <= y + h) {
                isDown = true;
            }
        }
    }

    /**
     * Draws the button on the passed batch
     * @param batch
     */
    public void draw(SpriteBatch batch) {
        if (! isDown) {
            batch.draw(textureUp, x, y, w, h);
        } else {
            batch.draw(textureDown, x, y, w, h);
        }
    }

    public boolean justPressed() {
        return isDown && !isDownPrev;
    }
}
