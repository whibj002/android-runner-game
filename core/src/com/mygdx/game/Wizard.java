package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Wizard {

    // Enemies
    private Vector2 delta;
    private Vector2 position;
    public Rectangle rectangle;
    private Animation animation;
    public float yPos;
    public float maxY;
    public float minY;

    private float moveSpeed = 100;

    private float timer;

    public Wizard (Vector2 delta, Vector2 position, Animation animation) {
        this.delta = delta;
        this.position = position;
        this.animation = animation;
        this.yPos = 548; // Same as pos in addWizard()
        this.maxY = yPos + 50;
        this.minY = yPos - 50;
    }

    public void render(SpriteBatch batch){
        batch.draw((TextureRegion) animation.getKeyFrame(timer, true), position.x, position.y);
    }

    public void update(float delta){
        timer += delta;
        position.x -= moveSpeed*delta;
        rectangle.x = position.x;
    }
}
