package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Bee {

    // Enemies
    private Vector2 beeDelta;
    private Vector2 beePosition;
    public Rectangle beeRectangle;
    private Animation beeAnimation;

    private float moveSpeed = 100;

    private float timer;

    /**
     * Bee constructor
     * @param beeDelta
     * @param beePosition
     * @param beeAnimation
     */
    public Bee (Vector2 beeDelta, Vector2 beePosition, Animation beeAnimation) {
        this.beeDelta = beeDelta;
        this.beePosition = beePosition;
        this.beeAnimation = beeAnimation;
    }

    /**
     * Renders the bee with the associated animation
     * @param batch
     */
    public void render(SpriteBatch batch){
        batch.draw((TextureRegion) beeAnimation.getKeyFrame(timer, true), beePosition.x, beePosition.y);
    }

    /**
     * Updates the bees position and rectangle position
     * @param delta
     */
    public void update(float delta){
        timer += delta;
        beePosition.x -= moveSpeed*delta;
        beeRectangle.x = beePosition.x;
    }
}
