package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Slime {

    // Enemies
    private Vector2 slimeDelta;
    private Vector2 slimePosition;
    public Rectangle slimeRectangle;
    private Animation slimeAnimation;

    private float moveSpeed = 100;

    private float timer;

    /**
     * Slime constructor
     * @param slimeDelta
     * @param slimePosition
     * @param slimeAnimation
     */
    public Slime (Vector2 slimeDelta, Vector2 slimePosition, Animation slimeAnimation) {
        this.slimeDelta = slimeDelta;
        this.slimePosition = slimePosition;
        this.slimeAnimation = slimeAnimation;
    }

    /**
     * Renders the slime
     * @param batch
     */
    public void render(SpriteBatch batch){
        batch.draw((TextureRegion) slimeAnimation.getKeyFrame(timer, true), slimePosition.x, slimePosition.y);
    }

    /**
     * Updates the slimes position and rectangle position
     * @param delta
     */
    public void update(float delta){
        timer += delta;
        slimePosition.x -= moveSpeed*delta;
        slimeRectangle.x = slimePosition.x;
    }
}
