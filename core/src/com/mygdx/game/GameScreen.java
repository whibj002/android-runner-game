package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

public class GameScreen implements Screen {

    private Player player;
    MyGdxGame game;

    // Music/sounds
    // bgMusic source: https://freesound.org/people/yummie/sounds/410574/
    Music bgMusic = Gdx.audio.newMusic(Gdx.files.internal("background-music.mp3"));
    // Jump and slide source: https://freesound.org/people/cabled_mess/sounds/350898/
    Sound jumpAndSlide = Gdx.audio.newSound(Gdx.files.internal("jump_and_slide.wav"));
    // Death source: https://freesound.org/people/jacksonacademyashmore/sounds/414209/
    Sound deathSound = Gdx.audio.newSound(Gdx.files.internal("death-sound.wav"));
    // Success source
    Sound winningSound = Gdx.audio.newSound(Gdx.files.internal("success-sound.wav"));

    // Player/Game info
    public enum GameState {PLAYING, COMPLETE, PAUSE, FAIL};
    GameState gameState;
    public enum Level {ONE, TWO};
    Level level;

    private int MOVEMENT_SPEED = 400;

    //Map and rendering
    private SpriteBatch batch;
    private SpriteBatch uiBatch;
    private SpriteBatch buttonBatch;
    private SpriteBatch playerDeadBatch;
    private SpriteBatch playerWinsBatch;

    private TiledMap tiledMap;
    private TiledMapRenderer tiledMapRenderer;
    private OrthographicCamera camera;

    //Map Properties
    private MapProperties mapProperties;
    private int mapWidth;
    private int mapHeight;
    private int tilePixelWidth;
    private int tilePixelHeight;
    int mapPixelWidth;
    int mapPixelHeight;

    // ENEMY - Slime
    Texture slimeSheet;
    private TextureRegion[] slimeFrames;
    private Animation slimeAnimation;
    // ENEMY - Bee
    Texture beeSheet;
    private TextureRegion[] beeeFrames;
    private Animation beeAnimation;
    // ENEMY CHICKEN
    Texture wizardSheet;
    private TextureRegion[] wizardFrames;
    private Animation wizardAnimation;

    //Buttons
    private Button jumpButton;
    private Button slideButton;
    private Button pauseButton;
    private Button resumeButton;
    private Texture transparent;
    private Texture pause;
    private Texture resume;
    private Button playAgain;
    private Texture playAgainTexture;
    private Button gameOver;
    private Texture gameOverTexture;
    private Button exitButton;
    private Texture exitTexture;
    private Button nextButton;
    private Texture nextTexture;
    private Button winButton;
    private Texture winTexture;

    //Game Clock
    private float dt;
    private float totalTime;
    private float stateTime;

    // Enemy lists
    List<Slime> slimes = new ArrayList<>();
    List<Bee> bees = new ArrayList<>();
    List<Wizard> wizards = new ArrayList<>();

    // Flags
    boolean isGamepaused = false;
    boolean winningSoundPlayed = false;

    // constructor to keep a reference to the main Game class
    public GameScreen(MyGdxGame game) {
        this.game = game;
    }

    public void create () {
        //Rendering
        // Main batch
        batch = new SpriteBatch();
        // Jump/slide buttons
        uiBatch = new SpriteBatch();
        // Transparent jump/slide
        uiBatch.setColor(Color.CLEAR);
        // Pause/new game buttons
        buttonBatch = new SpriteBatch();
        playerDeadBatch = new SpriteBatch();
        playerWinsBatch = new SpriteBatch();

        //TileMap
        tiledMap = new TmxMapLoader().load("level1.tmx");
        tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);
        mapProperties = tiledMap.getProperties();

        //Map Properties
        mapWidth = mapProperties.get("width", Integer.class);
        mapHeight = mapProperties.get("height", Integer.class);
        tilePixelWidth = mapProperties.get("tilewidth", Integer.class);
        tilePixelHeight = mapProperties.get("tileheight", Integer.class);
        mapPixelWidth = mapWidth * tilePixelWidth;
        mapPixelHeight = mapHeight * tilePixelHeight;

        //Camera
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 1000, 1000);

        //Button Texture
        transparent = new Texture(Gdx.files.internal("transparent-button.png"));
        pause = new Texture(Gdx.files.internal("pause-button.jpg"));
        playAgainTexture = new Texture(Gdx.files.internal("play-again-button.jpg"));
        gameOverTexture = new Texture(Gdx.files.internal("game-over.jpg"));
        resume = new Texture(Gdx.files.internal("resume-button.jpg"));
        winTexture = new Texture(Gdx.files.internal("you-win.png"));
        exitTexture = new Texture(Gdx.files.internal("exit-button.png"));
        nextTexture = new Texture(Gdx.files.internal("next-button.png"));

        //Jump/Slide Buttons
        float buttonSize = Gdx.graphics.getHeight() * 0.2f;
        jumpButton = new Button(0f, buttonSize*2, buttonSize*10, buttonSize*3, transparent, transparent);
        slideButton = new Button(250f, buttonSize/15, buttonSize*10, buttonSize*3, transparent, transparent);
        pauseButton = new Button(50f, 40, buttonSize, buttonSize/2, pause, pause);
        resumeButton = new Button(300f, 40, buttonSize, buttonSize/2, resume, resume);
        exitButton = new Button(Gdx.graphics.getWidth()/4, Gdx.graphics.getHeight()/3, buttonSize*2, buttonSize, exitTexture, exitTexture);
        nextButton = new Button(Gdx.graphics.getWidth()/5+750, Gdx.graphics.getHeight()/3, buttonSize*2, buttonSize, nextTexture,nextTexture);
        playAgain = new Button((float) (Gdx.graphics.getWidth()/1.9), (float) (Gdx.graphics.getHeight()/1.5), buttonSize*3, buttonSize,
                playAgainTexture, playAgainTexture);
        gameOver = new Button((float) (Gdx.graphics.getWidth()/5), (float) (Gdx.graphics.getHeight()/1.5), buttonSize*3, buttonSize,
                gameOverTexture, gameOverTexture);
        winButton = new Button((float) (Gdx.graphics.getWidth() / 4), Gdx.graphics.getHeight()/2, buttonSize*5, buttonSize*2, winTexture, winTexture);

        // Time tracking
        dt = 0.0f;
        totalTime = 0.0f;
        stateTime = 0.0f;

        newGame();
    }

    private void newGame() {
        // Play music
        bgMusic.play();
        bgMusic.setLooping(true);
        // Reset movement speed in case of death
        MOVEMENT_SPEED = 400;
        winningSoundPlayed = false;

        // Clear enemies from previous round(s)
        if (bees != null)
            bees.clear();
        if (slimes != null)
            slimes.clear();
        if (wizards != null)
            wizards.clear();

        // Player
        player = new Player(new Vector2(0,0), new Vector2(1, 410));
        player.playerDeltaRectangle = new Rectangle(player.playerPosition.x + (256-128)/2, player.playerPosition.y, 60, 151);
        // Reset players life status
        player.isDead = false;
        // Register Player animations
        player.runAnimation();
        player.jumpAnimation();
        player.slideAnimation();
        player.deadAnimation();

        // Enemy animations
        slimeAnimation();
        beeAnimation();
        chickenAnimation();

        if (level != Level.TWO){
            // Just manually placing enemies here for level 1. 1750 units apart, starting at 2500 on the X axis
            addSlime(2500);
            addBee(4250);
            addSlime(6000);
            addBee(7750);
            addSlime(9500);
            addBee(11250);
            addSlime(13000);
            addBee(14750);
            addSlime(16500);
            addBee(18250);
            addSlime(20000);

            /*
            I was supposed to make a second level with an additional wizard monster that moved up/down the y axis, but sorta ran out of time
            SO the second level is not much different to the first.
             */
        } else if (level == Level.TWO){
            // New positions for bees/slims but this  time with wizards too
            addSlime(2500);
            addWizard(4500);
            addBee(6500);
            addWizard(8250);
            addSlime(10000);
            addSlime(12000);
            addWizard(13750);
            addWizard(15000);
            addBee(16500);
            addSlime(18250);
            addWizard(20000);
        }

        // Start game
        gameState = GameState.PLAYING;
        player.playerState = Player.PlayerState.RUNNING;

        //Set camera position
        camera.position.x = camera.viewportWidth/2f;
        camera.position.y = camera.viewportHeight/2f + 250;

        // Reset time tracking variables
        dt = 0.0f;
        totalTime = 0.0f;
        stateTime = 0.0f;
    }

    private void update() {
        // Get delta time
        dt = Gdx.graphics.getDeltaTime();
        // Button check vars
        boolean checkTouch = Gdx.input.isTouched();
        int touchX = Gdx.input.getX();
        int touchY = Gdx.input.getY();

        switch (gameState) {
            case PLAYING:
                // Update buttons
                jumpButton.update(checkTouch, touchX, touchY);
                slideButton.update(checkTouch, touchX, touchY);
                pauseButton.update(checkTouch, touchX, touchY);

                // Update enemies
                for(Slime slime:slimes){
                    slime.update(Gdx.graphics.getDeltaTime());
                }
                for(Bee bee:bees){
                    bee.update(Gdx.graphics.getDeltaTime());
                }
                if (level == Level.TWO){
                    for (Wizard wiz:wizards){
                        wiz.update(Gdx.graphics.getDeltaTime());
                    }
                }

                // JUMP button
                if ((Gdx.input.isKeyPressed(Input.Keys.ANY_KEY) || jumpButton.isDown) && !player.jumpStarted && !player.isSliding){
                    jumpAndSlide.play();
                    // START TO JUMP START
                    player.playerState = Player.PlayerState.JUMP_START;
                    player.jumpStarted = true;
                    stateTime = 0;
                }
                // SLIDE button
                else if ((Gdx.input.isKeyPressed(Input.Keys.ANY_KEY) || slideButton.isDown) && !player.jumpStarted && !player.isSliding){
                    jumpAndSlide.play();
                    player.playerState = Player.PlayerState.SLIDE_START;
                    stateTime = 0;
                    player.isSliding = true;
                    // Lower height of rect while sliding
                    player.playerDeltaRectangle.height = 98;
                }
                // PAUSE button
                else if ((Gdx.input.isKeyPressed(Input.Keys.ANY_KEY) || pauseButton.isDown) && gameState == GameState.PLAYING){
                    pause();
                }

                // Player movements and Rectangle movements
                player.totalDistance += player.playerDelta.x;
                player.playerDelta.x = MOVEMENT_SPEED * dt;

                player.playerPosition.x += player.playerDelta.x;
                player.playerDeltaRectangle.x = player.playerPosition.x + (256-128)-2;
                player.playerDeltaRectangle.y = player.playerPosition.y;

                // Have camera follow player (until end of map)
                if (player.totalDistance < mapPixelWidth - 1000) {
                    camera.translate(player.playerDelta);
                    // End of map, player wins so state to COMPLETE
                } else {
                    gameState = GameState.COMPLETE;
                }

                // Call jump/slide funcs
                jump(Gdx.graphics.getDeltaTime(), player);
                slide(Gdx.graphics.getDeltaTime(), player.playerDelta.x, player);

                break;
            case PAUSE:
                resumeButton.update(checkTouch, touchX, touchY);
                // Check if "start" is pressed
                if ((Gdx.input.isKeyPressed(Input.Keys.ANY_KEY) || resumeButton.isDown) && gameState == GameState.PAUSE){
                    resume();
                }

                break;
            case FAIL:
                bgMusic.stop();
                // Update play again button
                playAgain.update(checkTouch, touchX, touchY);
                // Remove bees/slimes from lists for end of level

                if (Gdx.input.isKeyPressed(Input.Keys.ANY_KEY) || playAgain.isDown){
                    newGame();
                }

                break;
            case COMPLETE:
                // Have player run off screen
                player.playerPosition.x += 8;
                // Set player to running incase he was sliding/jumping at level end to avoid issues
                player.playerState = Player.PlayerState.RUNNING;
                // Ensure we only play the winning sound once
                if (!winningSoundPlayed){
                    winningSound.play();
                    winningSoundPlayed = true;
                }
                exitButton.update(checkTouch, touchX, touchY);
                nextButton.update(checkTouch, touchX, touchY);

                // EXIT
                if ((Gdx.input.isKeyPressed(Input.Keys.ANY_KEY) || exitButton.isDown) && gameState == GameState.COMPLETE){
                    dispose();
                    game.create();
                }
                // NEXT level
                if ((Gdx.input.isKeyPressed(Input.Keys.ANY_KEY) || nextButton.isDown) && gameState == GameState.COMPLETE){
                    level = Level.TWO;
                    newGame();
                }
                break;
        }
    }

    public void render(float f) {

        // Time variables
        totalTime += Gdx.graphics.getDeltaTime();
        dt = Gdx.graphics.getDeltaTime();
        stateTime += Gdx.graphics.getDeltaTime();

        // Call update func
        update();

        // Clear
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        player.playerTexture = (TextureRegion) player.runAnimation.getKeyFrame(stateTime, true);

        // Get current frames
        switch (player.playerState){
            case RUNNING:
                // Case for when game is paused so animation stops looping
                if (isGamepaused){
                    player.playerTexture = (TextureRegion) player.runAnimation.getKeyFrame(stateTime, false);
                } else {
                    player.playerTexture = (TextureRegion) player.runAnimation.getKeyFrame(stateTime, true);
                }
                break;
            case JUMP_START:
                if (isGamepaused){
                    player.playerTexture = (TextureRegion) player.jumpStartAnimation.getKeyFrame(stateTime, false);
                } else {
                    player.playerTexture = (TextureRegion) player.jumpStartAnimation.getKeyFrame(stateTime, true);
                }
                break;
            case JUMP_END:
                player.playerTexture = (TextureRegion) player.jumpEndAnimation.getKeyFrame(stateTime, false);
                break;
            case JUMPING:
                if (isGamepaused){
                    player.playerTexture = (TextureRegion) player.jumpingAnimation.getKeyFrame(stateTime, true);
                } else {
                    player.playerTexture = (TextureRegion) player.jumpingAnimation.getKeyFrame(stateTime, false);
                }
                break;
            case SLIDE_START:
                player.playerTexture = (TextureRegion) player.slideStartAnimation.getKeyFrame(stateTime, false);
                break;
            case SLIDE_END:
                player.playerTexture = (TextureRegion) player.slideEndAnimation.getKeyFrame(stateTime, false);
                break;
            case SLIDING:
                player.playerTexture = (TextureRegion) player.slidingAnimation.getKeyFrame(stateTime, false);
                break;
            case DEAD:
                player.playerTexture = (TextureRegion) player.deadAnimation.getKeyFrame(stateTime, false);
                break;
        }

        //Render the tilemap
        camera.update();
        tiledMapRenderer.setView(camera);
        tiledMapRenderer.render();

        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        // Main batch drawings
        // Draw player
        batch.draw(player.playerTexture, player.playerPosition.x, player.playerPosition.y);
        // Draw enemies
        for(Slime slime:slimes){
            slime.render(batch);
        }
        for(Bee bee:bees){
            bee.render(batch);
        }
        if (level == Level.TWO){
            for (Wizard w:wizards){
                w.render(batch);
            }
        }

        // End main batch
        batch.end();
        // New batches
        uiBatch.begin();
        buttonBatch.begin();
        playerDeadBatch.begin();
        playerWinsBatch.begin();

        // Collision check
        checkEnemyCollision();

        // PLAYING buttons
        switch (gameState){
            case PLAYING:
                jumpButton.draw(uiBatch);
                slideButton.draw(uiBatch);
                pauseButton.draw(buttonBatch);
                break;

            case PAUSE:
                resumeButton.draw(buttonBatch);
                break;

            case FAIL:
                playAgain.draw(playerDeadBatch);
                gameOver.draw(playerDeadBatch);
                break;

            case COMPLETE:
                winButton.draw(playerWinsBatch);
                nextButton.draw(playerWinsBatch);
                exitButton.draw(playerWinsBatch);
                break;
        }
        // End new batches
        uiBatch.end();
        buttonBatch.end();
        playerDeadBatch.end();
        playerWinsBatch.end();
    }

    // Custom functions below
    /**
     * Adds a slime to the tileset at the desired X position
     * @param posX
     */
    private void addSlime(float posX){

        float posY = 250+168; // 418

        float rectPosX = posX;
        float rectPosY = posY;
        float rectWidth = 80;
        float rectHeight = 56;
        Slime slime = new Slime(new Vector2(0,0), new Vector2(posX, posY), slimeAnimation);
        slime.slimeRectangle = new Rectangle(rectPosX, rectPosY, rectWidth, rectHeight);
        slimes.add(slime);
    }

    private void addWizard(float posX){

        float posY = 250+168 + 130; // 548

        float rectPosX = posX;
        float rectPosY = posY;
        float rectWidth = 40;
        float rectHeight = 40;
        Wizard wizard = new Wizard(new Vector2(0,0), new Vector2(posX, posY), wizardAnimation);
        wizard.rectangle = new Rectangle(rectPosX, rectPosY, rectWidth, rectHeight);
        wizards.add(wizard);
    }

    /**
     * Adds a bee to the tileset at the desired X position
     * @param posX
     */
    private void addBee(float posX){

        float posY = 250+168 + 130; // 548

        float rectPosX = posX;
        float rectPosY = posY;
        float rectWidth = 106;
        float rectHeight = 80;
        Bee bee = new Bee(new Vector2(0,0), new Vector2(posX, posY), beeAnimation);
        bee.beeRectangle = new Rectangle(rectPosX, rectPosY, rectWidth, rectHeight);
        bees.add(bee);
    }

    /**
     * Checks collisions between the player and enemies
     */
    private void checkEnemyCollision(){
        player.isHitting = false;
        for(Slime slime:slimes){
            if(player.playerDeltaRectangle.overlaps(slime.slimeRectangle) && !player.isDead){
                player.isHitting = true;
                MOVEMENT_SPEED = 0;
                player.playerState = Player.PlayerState.DEAD;
                stateTime = 0;
                player.isDead = true;
                gameState = GameState.FAIL;
                deathSound.play();
            }
        }

        for(Bee bee:bees){
            if(player.playerDeltaRectangle.overlaps(bee.beeRectangle) && !player.isDead){
                player.isHitting = true;
                MOVEMENT_SPEED = 0;
                player.playerState = Player.PlayerState.DEAD;
                stateTime = 0;
                player.isDead = true;
                gameState = GameState.FAIL;
                deathSound.play();
            }
        }
        if (level == Level.TWO){
            for (Wizard wizard : wizards){
                if (player.playerDeltaRectangle.overlaps(wizard.rectangle) && !player.isDead){
                    player.isHitting = true;
                    MOVEMENT_SPEED = 0;
                    player.playerState = Player.PlayerState.DEAD;
                    stateTime = 0;
                    player.isDead = true;
                    gameState = GameState.FAIL;
                    deathSound.play();
                }
            }
        }
    }

    /**
     * Moves the player up/down the Y axis at a predetermined speed and changes frames respectively
     * @param delta
     * @param player
     */
    private void jump(float delta, Player player){
        switch (player.playerState){
            case JUMP_START:
                player.jumpTimer += delta;
                if(player.jumpTimer >= player.jumpDelay){
                    player.playerState = Player.PlayerState.JUMPING;
                    player.ySpeed = player.JUMP_FORCE;
                    stateTime = 0;
                    player.jumpTimer = 0;
                }
                break;
            case JUMPING:
                player.jumpTimer += delta;

                if(player.jumpTimer >= player.jumpDelay){
                    player.jumpingFrames[0] = player.jumpStartFrames[2];
                }else{
                    player.jumpingFrames[0] = player.jumpStartFrames[1];
                }

                player.playerPosition.y += player.ySpeed*delta;
                player.ySpeed -= player.GRAVITY_FORCE*delta;

                if(player.ySpeed <= 0) {
                    player.playerState = Player.PlayerState.JUMP_END;
                    stateTime = 0;
                    player.jumpTimer = 0;
                }
                break;
            case JUMP_END:
                player.playerPosition.y += player.ySpeed*delta;
                player.ySpeed -= player.GRAVITY_FORCE*delta;

                if(player. playerPosition.y <= 410) {
                    player.playerPosition.y = 410;
                    player.playerState = Player.PlayerState.RUNNING;
                    player.jumpStarted = false;
                    stateTime = 0;
                }
                break;
        }
    }

    /**
     * Slides the player sprite
     * @param delta
     * @param playerXMove
     */
    private void slide(float delta, float playerXMove, Player player){

        switch (player.playerState){

            case SLIDE_START:
                player.slideTimer += delta;
                if(player.slideTimer >= player.slideDelay){
                    player.playerState = Player.PlayerState.SLIDING;
                    player.slideTimer = 0;
                }
                break;
            case SLIDING:
                player.slidedDistance += playerXMove;
                if(player.slidedDistance >= player.SLIDE_DISTANCE){
                    player.slidedDistance = 0;
                    player.playerState = Player.PlayerState.SLIDE_END;
                }
                break;
            case SLIDE_END:
                player.slideTimer += delta;
                if(player.slideTimer >= player.slideDelay){
                    player.playerState = Player.PlayerState.RUNNING;
                    player.slideTimer = 0;
                    player.isSliding = false;
                    player.playerDeltaRectangle.height = 151;
                }
                break;
        }
    }

    @Override
    public void dispose() {
        tiledMap.dispose();
        transparent.dispose();
        player.runSheet.dispose();
        bgMusic.dispose();
        jumpAndSlide.dispose();
        deathSound.dispose();
        batch.dispose();
        buttonBatch.dispose();
        uiBatch.dispose();
        playerDeadBatch.dispose();
        winningSound.dispose();
        playerWinsBatch.dispose();
    }

    // Slime animation
    public void slimeAnimation() {
        final int slimeCols = 2;
        final int slimeRows = 1;

        slimeSheet = new Texture(Gdx.files.internal("slime.png"));

        // Slime animation
        TextureRegion[][] temp = TextureRegion.split(slimeSheet, slimeSheet.getWidth() / slimeCols,
                slimeSheet.getHeight() / slimeRows);
        slimeFrames = new TextureRegion[slimeRows * slimeCols];
        int index = 0;
        for (int i = 0; i < slimeRows; i++) {
            for (int j = 0; j < slimeCols; j++){
                slimeFrames[index++] = temp[i][j];
            }
        }
        slimeAnimation = new Animation(0.2f, slimeFrames);
    }

    // Bee animation
    public void beeAnimation() {
        final int beeCols = 2;
        final int beeRows = 1;
        beeSheet = new Texture(Gdx.files.internal("bee.png"));

        // Slime animation
        TextureRegion[][] temp = TextureRegion.split(beeSheet, beeSheet.getWidth() / beeCols,
                beeSheet.getHeight() / beeRows);
        beeeFrames = new TextureRegion[beeRows * beeCols];
        int index = 0;
        for (int i = 0; i < beeRows; i++) {
            for (int j = 0; j < beeCols; j++){
                beeeFrames[index++] = temp[i][j];
            }
        }
        beeAnimation = new Animation(0.2f, beeeFrames);
    }

    public void chickenAnimation () {
        final int cols = 6;
        final int rows = 1;
        wizardSheet = new Texture(Gdx.files.internal("wizard fly forward.png"));

        TextureRegion[][] temp = TextureRegion.split(wizardSheet, wizardSheet.getWidth() / cols,
                wizardSheet.getHeight() / rows);
        wizardFrames = new TextureRegion[cols * rows];
        int index = 0;

        for (int i = 0; i < rows; i++){
            for (int j = 0; j < cols; j++){
                wizardFrames[index++] = temp[i][j];
            }
        }
        wizardAnimation = new Animation(0.2f, wizardFrames);
    }

    @Override
    public void resize(int width, int height) { }

    @Override
    public void pause() {
        gameState = GameState.PAUSE;
        isGamepaused = true;
    }

    @Override
    public void resume() {
        gameState = GameState.PLAYING;
        isGamepaused = false;
    }

    @Override
    public void show() {
        create();
    }

    @Override
    public void hide() { }
}
