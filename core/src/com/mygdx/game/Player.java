package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Player {

    public enum PlayerState {RUNNING, JUMP_START, JUMP_END, JUMPING, SLIDE_START, SLIDE_END, SLIDING, DYING, DEAD};
    PlayerState playerState;

    //Player Position
    public Vector2 playerDelta;
    public Vector2 playerPosition;
    public Rectangle playerDeltaRectangle;

    // Textures/Animations
    //RUN
    Texture runSheet;
    Animation runAnimation;

    // JUMP START
    Texture jumpStartSheet;
    Animation jumpStartAnimation;
    TextureRegion[] jumpStartFrames;

    // JUMP END
    Texture jumpEndSheet;
    Animation jumpEndAnimation;

    // JUMPING
    TextureRegion[] jumpingFrames;
    Animation jumpingAnimation;

    // SLIDE START
    Texture slideStartSheet;
    TextureRegion[] slideStartFrames;
    Animation slideStartAnimation;

    // SLIDE END
    Texture slideEndSheet;
    TextureRegion[] slideEndFrames;
    Animation slideEndAnimation;

    // SLIDING
    TextureRegion[] slidingFrames;
    Animation slidingAnimation;

    //DEAD
    Texture deadSheet;
    TextureRegion[] deadFrames;
    Animation deadAnimation;

    //Speed related numbers
    public final float JUMP_FORCE = 500;
    public final float SLIDE_DISTANCE = 300;
    public float slidedDistance = 0;
    public final float GRAVITY_FORCE = 1000;
    public float ySpeed = 0;

    //Animation numbers
    public float jumpDelay = 0.15f;
    public float slideDelay = 0.15f;
    public float jumpTimer = 0;
    public float slideTimer = 0;

    //Textures
    TextureRegion[] jumpStart;
    TextureRegion playerTexture;

    //Total distance travelled
    public float totalDistance;

    // Flags
    boolean jumpStarted;
    boolean isSliding;
    boolean isHitting = false;
    boolean isDead = false;

    /**
     *  Player constructor
     * @param playerDelta
     * @param playerPosition
     */
    public Player (Vector2 playerDelta, Vector2 playerPosition) {
        this.playerDelta = playerDelta;
        this.playerPosition = playerPosition;
    }

    /**
     * Player animation functions below
     */
    public void runAnimation() {
        final int runCols = 4;
        final int runRows = 2;
        TextureRegion[] runFrames;
        runSheet = new Texture(Gdx.files.internal("running.png"));
        // Player running animation
        TextureRegion[][] temp = TextureRegion.split(runSheet, runSheet.getWidth() / runCols, runSheet.getHeight() / runRows);
        runFrames = new TextureRegion[runRows * runCols];
        int index = 0;
        for (int i = 0; i < runRows; i++){
            for (int j = 0; j < runCols; j++) {
                runFrames[index++] = temp[i][j];
            }
        }
        runAnimation = new Animation(0.1f, runFrames);
    }

    public void deadAnimation() {
        final int deadCols = 3;
        final int deadRows = 1;
        deadSheet = new Texture(Gdx.files.internal("deading.png"));

        // Slime animation
        TextureRegion[][] temp = TextureRegion.split(deadSheet, deadSheet.getWidth() / deadCols,
                deadSheet.getHeight() / deadRows);
        deadFrames = new TextureRegion[deadRows * deadCols];
        int index = 0;
        for (int i = 0; i < deadRows; i++) {
            for (int j = 0; j < deadCols; j++){
                deadFrames[index++] = temp[i][j];
            }
        }
        deadAnimation = new Animation(0.25f, deadFrames);
    }

    public void slideAnimation() {
        final int slideStartCols = 2;
        final int slideStartRows = 1;

        final int slideEndCols = 1;
        final int slideEndRows = 1;

        slideStartSheet = new Texture(Gdx.files.internal("sliding start.png"));
        slideEndSheet = new Texture(Gdx.files.internal("sliding end.png"));

        TextureRegion[][] start = TextureRegion.split(slideStartSheet, slideStartSheet.getWidth() / slideStartCols,
                slideStartSheet.getHeight() / slideStartRows);
        TextureRegion[][] end = TextureRegion.split(slideEndSheet, slideEndSheet.getWidth() / slideEndCols,
                slideEndSheet.getHeight() / slideEndRows);
        slideStartFrames = new TextureRegion[slideStartRows * slideStartCols];
        slideEndFrames = new TextureRegion[slideEndRows * slideEndCols];

        // Start
        int startIndex = 0;
        for (int i = 0; i < slideStartRows; i++) {
            for (int j = 0; j < slideStartCols; j++) {
                slideStartFrames[startIndex++] = start[i][j];
            }
        }
        // End
        int endIndex = 0;
        for (int i = 0; i < slideEndRows; i++){
            for (int j = 0; j < slideEndCols; j++){
                slideEndFrames[endIndex++] = end[i][j];
            }
        }

        slidingFrames = new TextureRegion[1];
        slidingFrames[0] = slideStartFrames[1];

        slideStartAnimation = new Animation(0.166f, slideStartFrames);
        slideEndAnimation = new Animation(0.166f, slideEndFrames);
        slidingAnimation = new Animation(0.166f, slidingFrames);
    }

    public void jumpAnimation() {
        final int jumpStartCols = 3;
        final int jumpStartRows = 1;

        final int jumpEndCols = 2;
        final int jumpEndRows = 1;


        jumpStartSheet = new Texture(Gdx.files.internal("jumping start.png"));
        jumpEndSheet = new Texture(Gdx.files.internal("jumping end.png"));

        TextureRegion[][] start = TextureRegion.split(jumpStartSheet, jumpStartSheet.getWidth() / jumpStartCols,
                jumpStartSheet.getHeight() / jumpStartRows);
        TextureRegion[][] end = TextureRegion.split(jumpEndSheet, jumpEndSheet.getWidth() / jumpEndCols,
                jumpEndSheet.getHeight() / jumpEndRows);
        jumpStartFrames = new TextureRegion[jumpStartRows * jumpStartCols];
        TextureRegion[] jumpEndFrames = new TextureRegion[jumpEndRows * jumpEndCols];

        // Start
        int startIndex = 0;
        for (int i = 0; i < jumpStartRows; i++) {
            for (int j = 0; j < jumpStartCols; j++) {
                jumpStartFrames[startIndex++] = start[i][j];
            }
        }
        // End
        int endIndex = 0;
        for (int i = 0; i < jumpEndRows; i++){
            for (int j = 0; j < jumpEndCols; j++){
                jumpEndFrames[endIndex++] = end[i][j];
            }
        }

        jumpingFrames = new TextureRegion[1];
        jumpingFrames[0] = jumpStartFrames[2];

        jumpingAnimation = new Animation(0.166f, jumpingFrames);
        jumpStartAnimation = new Animation(0.166f, jumpStartFrames);
        jumpEndAnimation = new Animation(0.166f, jumpEndFrames);
    }
}
