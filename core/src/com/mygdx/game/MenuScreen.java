package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class MenuScreen implements Screen {

    MyGdxGame game;

    private SpriteBatch batch;
    private Skin skin;
    private Stage stage;

    // Buttons
    private TextButton startButton;
    private TextButton exitButton;

    // TITLE FONT
    FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font/Boldfinger.ttf"));
    FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
    BitmapFont font24;

    // MUSIC source: https://freesound.org/people/joshuaempyre/sounds/251461/
    Music menuMusic = Gdx.audio.newMusic(Gdx.files.internal("main-menu-music.wav"));

    // constructor to keep a reference to the main Game class
    public MenuScreen(MyGdxGame game) {
        this.game = game;
    }

    public void create() {
        menuMusic.play();
        menuMusic.setLooping(true);
        batch = new SpriteBatch();
        skin = new Skin(Gdx.files.internal("craftacular/skin/craftacular-ui.json"));
        stage = new Stage();

        // Start + Exit buttons
        startButton = new TextButton("Start", skin, "default");
        exitButton = new TextButton("Exit", skin, "default");

        // Title/author fonts
        parameter.size = 150;
        parameter.borderWidth = 1;
        parameter.color = Color.BLACK;
        parameter.shadowOffsetX = 3;
        parameter.shadowOffsetY = 3;
        parameter.shadowColor = new Color(0.1f, 0, 0, 0.75f);
        font24 = generator.generateFont(parameter);
        generator.dispose();
        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.font = font24;

        Label title = new Label("Endless Runner",labelStyle);
        title.setSize(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
        title.setPosition(Gdx.graphics.getHeight()-600, 550);

        Label author = new Label("By Ben Whitely", labelStyle);
        author.setFontScale(0.5f);
        author.setSize(Gdx.graphics.getWidth()/4, Gdx.graphics.getHeight()/4);
        author.setPosition(Gdx.graphics.getHeight()-300, 50);

        stage.addActor(title);
        stage.addActor(author);

        // Adjust font size on menu buttons
        startButton.getLabel().setFontScaleX(startButton.getLabel().getFontScaleX() + 2f);
        startButton.getLabel().setFontScaleY(startButton.getLabel().getFontScaleY() + 2f);
        exitButton.getLabel().setFontScaleX(exitButton.getLabel().getFontScaleX() + 2f);
        exitButton.getLabel().setFontScaleY(exitButton.getLabel().getFontScaleY() + 2f);

        // Set height/widths on menu buttons
        startButton.setWidth(450f);
        startButton.setHeight(300f);
        exitButton.setWidth(450f);
        exitButton.setHeight(300f);

        // Set positions and add actors to stage
        startButton.setPosition(Gdx.graphics.getWidth() / 2 - 550f, Gdx.graphics.getHeight()/2 - 200f);
        exitButton.setPosition(Gdx.graphics.getWidth()/2 + 250f, Gdx.graphics.getHeight()/2 - 200f);
        stage.addActor(startButton);
        stage.addActor(exitButton);
        Gdx.input.setInputProcessor(stage);

        // Click listener from clicking "Start" to entering Game screen
        startButton.addListener(new ClickListener()
        {
            @Override
            public void clicked (InputEvent event, float x, float y)
            {
                game.setScreen(MyGdxGame.gameScreen);
                dispose();
            }
        });

        // Exit back to OS
        exitButton.addListener(new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y)
            {
                Gdx.app.exit();
                System.exit(-1);
            }
        });
    }

    @Override
    public void show() {
        create();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1,1,1,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        stage.draw();
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        batch.dispose();
        menuMusic.dispose();
    }
}
